srish.controller("marketerBudgetsController", [ '$scope', '$route', '$http',
		'$rootScope', 'OAuth', '$location',
		function($scope, $route, $http, $rootScope, OAuth, $location) {
			$scope.headerTemplate = "views/marketer/marketerHeader.html";
			// for page init
			$scope.pageInit = function() {
				$http.get('budgets/getTotalAmountSpent').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.totalAmount = response.result;
							} else {
								$scope.totalAmount = 0;
							}
						});
				
				$http.get('budgets/getAllActiveBudgets').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.allBudgets = response.result;
							} else {
								$scope.allBudgets = null;
							}
						});
			};

		} ]);