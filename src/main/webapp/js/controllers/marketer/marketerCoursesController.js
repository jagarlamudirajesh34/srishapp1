srish.controller("marketerCoursesController", [
		'$scope',
		'$route',
		'$http',
		'$rootScope',
		'OAuth',
		'$location',
		function($scope, $route, $http, $rootScope, OAuth, $location) {
			$scope.headerTemplate = "views/marketer/marketerHeader.html";

			// for page init
			$scope.pageInit = function() {
				$http.get('courses/getAllActiveCourses').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.allActiveCourses = response.result;
							} else {
								$scope.allActiveCourses = null;
							}
						});
			};

		} ]);