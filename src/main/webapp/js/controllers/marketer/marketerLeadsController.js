srish.controller("marketerLeadsController", [
		'$scope',
		'$route',
		'$http',
		'$rootScope',
		'OAuth',
		'$location',
		function($scope, $route, $http, $rootScope, OAuth, $location) {
			$scope.headerTemplate = "views/marketer/marketerHeader.html";
			$scope.mobilePattern = /^[0-9]+$/;
			$scope.id = 0;
			 $('.datepicker').datepicker({
		         /*startDate: new Date(),
		         endDate: new Date(),*/
		         autoclose: true
		     });
			// for page init
			$scope.pageInit = function() {
				$scope.addPage = false;
				$scope.viewPage = true;
				$scope.editPage = false;
				$scope.errorMsg = "Select any course for display leads";
				$http.get('courses/getAllActiveCourses').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.allCourses = response.result;
							} else {
								$scope.allCourses = null;
							}
						});
			};
            
			//add lead button function
			$scope.addLead = function(){
				$scope.addPage = true;
				$scope.viewPage = false;
				$scope.editPage = false;
			};
			
			//Cancel lead button function
			$scope.cancel = function(){
				$scope.addPage = false;
				$scope.viewPage = true;
				$scope.editPage = false;
				$scope.name = "";
				$scope.mobile = "";
				$scope.mailid = "";
				$scope.whatsappp = "";
				$scope.selectedCourse = "";
				$scope.showNameValidationMessage = false;
				$scope.showEmailValidationMessage = false;
				$scope.showMobileValidationMessage = false;
				$scope.showWhatsappValidationMessage = false;
				$scope.showSelectCourseValidationMessage = false;	
			};
			
			//SUbmit lead button function
			$scope.submitLead = function(){
				$scope.addPage = false;
				$scope.viewPage = true;
				$scope.editPage = false;
			};
			
			$scope.devOps = function() {
				$scope.enqListName = "DevOps Leads List";
				$http.get('leads/getAllActiveLeads/devops').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.allLeads = response.result;
								$scope.errorMsg = "";
							} else {
								$scope.allLeads = null;
								$scope.errorMsg = "No Leads found for this course";
							}
						});
			}

			$scope.salesforce = function() {
				$scope.enqListName = "Salesforce Leads List";
				$http.get('leads/getAllActiveLeads/salesforce').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.allLeads = response.result;
								$scope.errorMsg = "";
							} else {
								$scope.allLeads = null;
								$scope.errorMsg = "No Leads found for this course";
							}
						});
			}

			$scope.mule = function() {
				$scope.enqListName = "Mule Leads List";
				$http.get('leads/getAllActiveLeads/mule').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.allLeads = response.result;
								$scope.errorMsg = "";
							} else {
								$scope.allLeads = null;
								$scope.errorMsg = "No Leads found for this course";
							}
						});
			}

			$scope.serviceNow = function() {
				$scope.enqListName = "ServiceNow Leads List";
				$http.get('leads/getAllActiveLeads/servicenow').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.allLeads = response.result;
								$scope.errorMsg = "";
							} else {
								$scope.allLeads = null;
								$scope.errorMsg = "No Leads found for this course";
							}
						});
			};
			
			//On selection of course
			$scope.courseSelected = function(selectedCourse){
				if(selectedCourse.id != null){
					$scope.selectedCourseId = selectedCourse.id;
				}
			};
			
			//when click on submit function
			$scope.submitLead = function(){
				if($scope.name == null || $scope.name == undefined || $scope.name == ""){
					$scope.showNameValidationMessage = true;
					$scope.nameValidationMessage = "Please enter name";
					return false;
				}else{
					$scope.showNameValidationMessage = false;
				}
				if($scope.mailid == null || $scope.mailid == undefined || $scope.mailid == ""){
					$scope.showEmailValidationMessage = true;
					$scope.emailValidationMessage = "Please enter mail";
					return false;
				}else{
					$scope.showEmailValidationMessage = false;
				}
				if($scope.mobile == null || $scope.mobile == undefined || $scope.mobile == "" || $scope.mobile.length != 10 || !$scope.mobilePattern.test($scope.mobile)){
					$scope.showMobileValidationMessage = true;
					$scope.mobileValidationMessage = "Please enter 10 digit mobile";
					return false;
				}else{
					$scope.showMobileValidationMessage = false;
				}
				if($scope.whatsapp == null || $scope.whatsapp == undefined || $scope.whatsapp == "" || $scope.whatsapp.length != 10 || !$scope.mobilePattern.test($scope.whatsapp)){
					$scope.showWhatsappValidationMessage = true;
					$scope.whatsappValidationMessage = "Please enter 10 digit whatsapp number";
					return false;
				}else{
					$scope.showWhatsappValidationMessage = false;
				}
				if($scope.selectedCourseId == 0 || $scope.selectedCourseId == null || $scope.selectedCourseId == "" || $scope.selectedCourseId == undefined){
					$scope.showSelectCourseValidationMessage = true;
					$scope.selectCourseValidationMessage = "Select any course";
					return false;
				}else{
					$scope.showSelectCourseValidationMessage = false;
				}
				
				var obj = {
					name: $scope.name,
					mobile: $scope.mobile,
					mail: $scope.mailid,
					whatsapp: $scope.whatsapp,
					enquiryBy: $rootScope.appLoginUser.name,
					deletedYn: false,
					course : {
						id: $scope.selectedCourseId
					}
				}
				$http({	url : "leads/saveLead",
					headers : {
					  'Content-Type' : 'application/json',
					  'Accept' : 'application/json'
					},
					method : "POST",
					data : obj
			    }).success(function(response, status,headers) {
				    if (response.status == 'success') {
				    	$scope.cancel();
				    	if($scope.selectedCourseId == 1){
				    		$scope.devOps();
				    	}else if($scope.selectedCourseId == 2){
				    		$scope.salesforce();
				    	}else if($scope.selectedCourseId == 3){
				      		$scope.serviceNow();
				    	}else if($scope.selectedCourseId == 4){
				    		$scope.mule();
				    	}
				    }
			    });
			};
			//Edit Lead functions
			$scope.editLead = function(eLead){
				$scope.cId = eLead.course.id;
				$scope.eLead = eLead;
				$scope.editPage = true;
				$scope.viewPage = false;
				$scope.addPage = false;
				$scope.showEditNameValidationMessage = false;
				$scope.showEditEmailValidationMessage = false;
				$scope.showEditMobileValidationMessage = false;
				$scope.showEditWhatsappValidationMessage = false;
				$scope.showEditSelectCourseValidationMessage = false;
				$scope.showEditStaffCommentsValidationMessage = false;
			};
			
			//cancel Edit page
			$scope.cancelEdit = function(){
				if($scope.cId == 1){
		    		$scope.devOps();
		    	}else if($scope.cId == 2){
		    		$scope.salesforce();
		    	}else if($scope.cId == 3){
		      		$scope.serviceNow();
		    	}else if($scope.cId == 4){
		    		$scope.mule();
		    	}
				$scope.addPage = false;
				$scope.viewPage = true;
				$scope.editPage = false;
			};
			
			//Submit Edit Lead
			$scope.submitEditLead = function(){
				if($scope.eLead.name == "" || $scope.eLead.name == null || $scope.eLead.name == undefined){
					$scope.editNameValidationMessage = "Enter name";
					$scope.showEditNameValidationMessage = true;
					return false;
				}else{
					$scope.showEditNameValidationMessage = false;
				}
				if($scope.eLead.mail == "" || $scope.eLead.mail == null || $scope.eLead.mail == undefined){
					$scope.editEmailValidationMessage = "Enter valid mailid";
					$scope.showEditEmailValidationMessage = true;
					return false;
				}else{
					$scope.showEditEmailValidationMessage = false;
				}
				if($scope.eLead.mobile == null || $scope.eLead.mobile == undefined || $scope.eLead.mobile == "" || $scope.eLead.mobile.length != 10 || !$scope.mobilePattern.test($scope.eLead.mobile)){
					$scope.showEditMobileValidationMessage = true;
					$scope.editMobileValidationMessage = "Please enter 10 digit mobile";
					return false;
				}else{
					$scope.showEditMobileValidationMessage = false;
				}
				if($scope.eLead.whatsapp == null || $scope.eLead.whatsapp == undefined || $scope.eLead.whatsapp == "" || $scope.eLead.whatsapp.length != 10 || !$scope.mobilePattern.test($scope.eLead.whatsapp)){
					$scope.showEditWhatsappValidationMessage = true;
					$scope.editWhatsappValidationMessage = "Please enter 10 digit whatsapp number";
					return false;
				}else{
					$scope.showEditWhatsappValidationMessage = false;
				}
				if($scope.editSelectedCourseId == "" || $scope.editSelectedCourseId == null || $scope.editSelectedCourseId == undefined){
					$scope.showEditSelectCourseValidationMessage = true;
					$scope.selectEditCourseValidationMessage = "Select any course";
					return false;
				}else{
					$scope.showEditSelectCourseValidationMessage = false;
				}
				
				
				
				
				
			};

			//Edit COurse Selected changes
			$scope.editCourseSelected = function(eCorse){
				console.log(eCorse);
				$scope.editSelectedCourseId = null;
				if(eCorse.id != null){
					$scope.editSelectedCourseId = eCorse.id;
				}
			};
			
		} ]);