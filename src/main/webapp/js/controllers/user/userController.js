srish.controller("userController",[
				'$scope',
				'$route',
				'$http',
				'$rootScope',
				'OAuth',
				'$location',
				function($scope, $route, $http, $rootScope, OAuth,$location){
					$scope.headerTemplate = "views/header.html";
					
					//for getting all active user types
					/*$scope.getUsertype = function(){
						$http.get('util/getAllActiveRoles')
						.success(function(response, status,	headers) {
									if (response.status == 'success') {
										$scope.allRolesList = response.result;
										console.log($scope.allRolesList);
									} else {
										$scope.allRolesList = null;
									}
								});				
					}	*/
					
					
					/*$scope.userTypeSelected = function(selectedUserType){
						
						
					}*/
					
	    $scope.loginUser = function() {
		var mobile = $scope.mobile;
		var password = $scope.password;
		//var usertype = $scope.usertype;
		$scope.loginFailure = false;
		$scope.loginErrorMessage = "Invalid credentails";
		if(mobile == null){
			$scope.loginErrorMessage = "Please enter mobile number";
		}else if($scope.mobile.length != 10){
			$scope.loginErrorMessage = "Please enter 10 digit mobile number";
		}else if($scope.password == null || $scope.password == "" || $scope.password == undefined){
			$scope.loginErrorMessage = "Please enter password";
		}
		$rootScope.$on('oauth:error', function(event, rejection) {
		          var rejectionData;
		          
		          if(angular.isString(rejection.data)) {
		           rejectionData = JSON.parse(rejection.data);
		          } else {
		           $scope.loginFailure = true;
		           rejectionData = rejection.data;
		          }
		             if ('invalid_grant' === rejectionData.error) {
						 $scope.loginFailure = true;
		               return;
		             }
		             return $location.path('/index');
		           });
			
			   var userData = {};
			   userData['username'] = mobile;
			   userData['password'] = password;
		       userData['scope'] = 'read write';
		       //userData['userType'] = $scope.selectedUserType.userType;
		       
		       var options = {};
		       var headers = {};
		       
		       headers['Authorization'] = 'Basic '+Base64.encode('trackmykid:trackmykid');
		       headers['Accept'] = 'application/json';
		       headers['Content-Type'] = 'application/x-www-form-urlencoded';
		       
		       options['headers'] = headers;
		       OAuth.getAccessToken(userData, options).then(function(response) {
		    	   $http
					.post('user/checkSession',
							{
								headers:{"Content-Type": "application/json",
										"Accept": "application/json"}
							})
					.success(function(response, status, headers) {
						if (response.status == 'success') {
							$scope.loginFailure = false;
							var appLoginUser = response.result;
							$rootScope.appLoginUser = appLoginUser;
							if(appLoginUser.userType == 'admin') {
								$scope.$parent.headerTemplate = "views/admin/adminHeader.html";
								$location.path(appLoginUser.userType + "/home/");
							}else if(appLoginUser.userType == 'marketer') {
								$scope.$parent.headerTemplate = "views/marketer/marketerHeader.html";
								$location.path($scope.usertype + "/home/");
							}else if(appLoginUser.userType == 'accountant') {
								$scope.$parent.headerTemplate = "views/accountant/accountantHeader.html";
								$location.path($scope.usertype + "/home/");
							}
						    } else {
							   $scope.loginFailure = true;
							   $scope.loginErrorMessage = "Invalid Credentials!!";
						}
					});
		    	   
		       });

	};
}]);