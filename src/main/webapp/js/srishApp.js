var srish = angular.module("srish", [ 'ngCookies', 'ngRoute',
		'http-auth-interceptor', 'angular-oauth2','ui.bootstrap', 'ngFileUpload', 'ngAnimate', 'ngSanitize','ui.grid', 'ui.grid.edit', 'ui.grid.cellNav', 'ui.grid.selection', 'ui.grid.exporter', 'ui.select', 'ngSanitize', 'dx']);

srish.config(function($routeProvider) {
	$routeProvider
	// Routing for User Template
	.when("/index", {
		controller : 'userController',
		templateUrl : 'views/home.html'
	}).when("/admin/home", {
		controller : 'adminHomeController',
		templateUrl : 'views/admin/adminHome.html'
	}).when("/admin/profile", {
		controller : 'adminUsersController',
		templateUrl : 'views/admin/adminUsers.html'
	}).when("/admin/users", {
		controller : 'adminUsersController',
		templateUrl : 'views/admin/adminUsers.html'
	})
	
	
	
	.when("/marketer/home", {
		controller : 'marketerHomeController',
		templateUrl : 'views/marketer/marketerHome.html'
	}).when("/marketer/profile", {
		controller : 'marketerProfileController',
		templateUrl : 'views/marketer/marketerProfile.html'
	}).when("/marketer/leads", {
		controller : 'marketerLeadsController',
		templateUrl : 'views/marketer/marketerLeads.html'
	}).when("/marketer/budgets", {
		controller : 'marketerBudgetsController',
		templateUrl : 'views/marketer/marketerBudgets.html'
	}).when("/marketer/courses", {
		controller : 'marketerCoursesController',
		templateUrl : 'views/marketer/marketerCourses.html'
	}).when("/marketer/sms", {
		controller : 'marketerSmsController',
		templateUrl : 'views/marketer/marketerSms.html'
	}).when("/marketer/reports", {
		controller : 'marketerReportsController',
		templateUrl : 'views/marketer/marketerReports.html'
	}).when("/marketer/followup", {
		controller : 'marketerFollowupController',
		templateUrl : 'views/marketer/marketerFollowup.html'
	})
	
	
	.when("/admin/requests", {
		controller : 'adminRequestsController',
		templateUrl : 'views/admin/adminRequests.html'
	})
	
	
	.when("/admin/allBanksCashbooks", {
		controller : 'adminBankCashbookController',
		templateUrl : 'views/admin/adminBankCashbook.html'
	}).when("/admin/allGroupsLedgers", {
		controller : 'adminGroupsLedgerController',
		templateUrl : 'views/admin/adminGroupsLedger.html'
	}).when("/admin/allUsersCashbooks", {
		controller : 'adminAllUsersCashbookController',
		templateUrl : 'views/admin/adminAllUsersCashbook.html'
	})
	
	
	
	
	
	.when("/user/forgetPassword", {
		controller : 'forgetPasswordController',
		templateUrl : 'views/forgetPassword.html'
	})
	
	.when("/admin/test", {
		controller : 'testController',
		templateUrl : 'views/admin/test.html'
	}).when("/admin/ledgerMaster", {
		controller : 'adminLedgerMasterController',
		templateUrl : 'views/admin/adminLedgerMaster.html'
	})
	
	
		
	// Otherwise:
	.otherwise({
		redirectTo : '/index'
	})
	

})

srish
		.run([
				'$rootScope',
				'$window',
				'$location',
				'OAuth',
				'OAuthToken',
				'authService',
				function($rootScope, $window, $location, OAuth, OAuthToken,
						authService) {

					$rootScope
							.$on(
									"$locationChangeStart",
									function(event, next, current) {

										var currentPath = null;
										var nextPath = null;

										if (angular.isDefined(current)
												&& current.indexOf('#/') > -1)
											currentPath = current
													.substring(current
															.indexOf('#/') + 1);

										if (angular.isDefined(next)
												&& next.indexOf('#/') > -1)
											nextPath = next.substring(next
													.indexOf('#/') + 1);

										if (!OAuth.isAuthenticated()) {

											delete $rootScope.appLoginUser;
											// alert("sampleApp.js - OAuth is not authenticated.");

											if (nextPath != null
													&& (nextPath.startsWith('/admin')
															|| nextPath.startsWith('/marketer') 
															|| nextPath.startsWith('/accountant'))) {
												$location.path('index');
												angular.element(
														$("#mainTemplate"))
														.scope().headerTemplate = "views/header.html";

												return;
											}
										}else{
											//alert("else in sampleApp.js");
										}

										var appLoginUser = $rootScope.appLoginUser;

										if (angular.isDefined(appLoginUser)) {
											//alert("defined == in sampleApp.js");
											if (appLoginUser.userType == 'admin'
													&& (nextPath == null 
															|| (!nextPath.startsWith('/admin')))) {
												if (currentPath != null
														&& currentPath.startsWith('/admin')) {
													$location.path(currentPath);
												} else {
													$location.path('/admin/dashboard');
												}

											}else if (appLoginUser.userType == 'marketer'
												&& (nextPath == null 
														|| (!nextPath.startsWith('/marketer')))) {
											if (currentPath != null
													&& currentPath.startsWith('/marketer')) {
												$location.path(currentPath);
											} else {
												$location.path('/marketer/home');
											}

										} else if (appLoginUser.userType == 'manager'
													&& (nextPath == null || !nextPath.startsWith('/manager'))) {
													if (currentPath != null
															&& (currentPath.startsWith('/manager')))
														$location.path(currentPath);
													else
														$location.path('/manager/fleetManagement');
												
											}
										}

									});

					$rootScope
							.$on(
									'oauth:error',
									function(event, rejection) {
										var rejectionData;

										if (angular.isString(rejection.data)) {
											rejectionData = JSON
													.parse(rejection.data);
										} else {
											rejectionData = rejection.data;
										}

										// Ignore `invalid_grant` error - should
										// be catched on `LoginController`.
										if ('invalid_grant' === rejectionData.error) {
											if (angular
													.isDefined(rejectionData.error_description)
													&& rejectionData.error_description
															.indexOf('Invalid refresh token') > -1) {
												$location.path('index');
											} else
												return;
										}

										// Refresh token when a `invalid_token`
										// error occurs.
										if ('invalid_token' === rejectionData.error) {

											var options = {};
											var headers = {};

											headers['Authorization'] = 'Basic '
													+ Base64
															.encode('trackmykid:trackmykid');
											headers['Accept'] = 'application/json';
											headers['Content-Type'] = 'application/x-www-form-urlencoded';

											options['scope'] = 'read write';
											options['headers'] = headers;

											OAuth
													.getRefreshToken(null,
															options)
													.then(
															function(response) {
																var token = OAuthToken
																		.getToken();
																authService
																		.loginConfirmed(
																				'success',
																				function(
																						config) {
																					config.headers["Authorization"] = 'Bearer '
																							+ token.access_token;
																					return config;
																				});
															});
											return;
										}

										// Redirect to `/login` with the
										// `error_reason`.
										return $location.path('index');
									});

					$rootScope.user = {};

					// $rootScope.gdata = GData;

					var CLIENT = '311395685548-1460djek3uelogitknq2poghbnuiuka3.apps.googleusercontent.com';
					var BASE;
					if ($window.location.hostname == 'localhost') {
						BASE = '//localhost:8080/_ah/api';
					} else {
						//BASE = 'http://183.101.82.5:9080/_ah/api';
					}

					BASE = 'https://cloud-endpoints-gae.appspot.com/_ah/api';

				} ]);

srish.factory('authorizationInterceptor', function() {
	return {
		request : function(config) {
			if (angular.isDefined(config.method) && config.method === 'GET')
				delete config.headers.Authorization;
			return config;
		}
	};
})

srish.config([ 'OAuthProvider', 'OAuthTokenProvider', '$httpProvider',
		function(OAuthProvider, OAuthTokenProvider, $httpProvider) {
			OAuthProvider.configure({
				baseUrl : 'http://' + location.host + contextPath,
				grantPath : '/oauth/token',
				clientId : 'trackmykid',
				clientSecret : 'trackmykid'
			});
			OAuthTokenProvider.configure({
				name : 'token',
				options : {
					secure : false
				}
			});

			$httpProvider.interceptors.push('authorizationInterceptor');

		} ]);
