package com.srishapp.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.srishapp.modal.Enquiry;
import com.srishapp.service.LeadsService;
import com.srishapp.util.ServiceStatus;

@RestController
@RequestMapping("/leads")
public class LeadsController {

	@Autowired
	LeadsService leadsService;

	// for getting all Active courses details
	@RequestMapping(value = "/getAllActiveLeads/{course}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveCourses(@PathVariable("course") String course) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Enquiry> allActiveEnquiry = leadsService.getAllActiveLeads(course);
			if (allActiveEnquiry != null && !allActiveEnquiry.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setMessage("Enquiry list retrived successfully");
				serviceStatus.setResult(allActiveEnquiry);
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No list found");
			}
		} catch (Exception e) {
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for sending sms to all parents
	@RequestMapping(value = "/saveLead", method = RequestMethod.POST, produces = { "application/json" }, consumes = {
			"application/json" })
	public ServiceStatus saveLead(@RequestBody Enquiry enquiry) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			leadsService.saveLead(enquiry);
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("saved lead submitted successfully");
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}
}
