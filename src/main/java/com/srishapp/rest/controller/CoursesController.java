package com.srishapp.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.srishapp.modal.Courses;
import com.srishapp.service.CoursesService;
import com.srishapp.util.ServiceStatus;

@RestController
@RequestMapping("/courses")
public class CoursesController {

	@Autowired
	CoursesService coursesService;

	// for getting all Active courses details
	@RequestMapping(value = "/getAllActiveCourses", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveCourses() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Courses> allActiveCourses = coursesService.getAllActiveCourses();
			if (allActiveCourses != null && !allActiveCourses.isEmpty()) {
				serviceStatus.setMessage("Retrived all courses list successfully");
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allActiveCourses);
			} else {
				serviceStatus.setMessage("No records found");
				serviceStatus.setStatus("failure");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setMessage("execption occured");
			serviceStatus.setStatus("failure");
		}
		return serviceStatus;
	}

}
