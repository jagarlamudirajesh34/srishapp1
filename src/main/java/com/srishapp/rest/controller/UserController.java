package com.srishapp.rest.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.srishapp.modal.User;
import com.srishapp.security.UserRepositoryUserDetails;
import com.srishapp.service.UserService;
import com.srishapp.service.UserServiceImpl;
import com.srishapp.util.ServiceStatus;

@RestController
@RequestMapping("/user")
public class UserController {

	private TokenStore tokenStore;

	@Autowired
	UserService userService;

	@RequestMapping(value = "/checkSession", method = RequestMethod.POST, produces = {
			"application/json" }, consumes = { "application/json" })
	public ServiceStatus checkToken(@RequestHeader(name = "Authorization", required = true) String authHeaderToken) {
		ServiceStatus serviceStatus = new ServiceStatus();
		serviceStatus.setStatus("success");
		serviceStatus.setMessage("Valid user session..");
		try {
			if (tokenStore == null) {
				ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
						.getRequestAttributes();
				WebApplicationContext applicationContext = WebApplicationContextUtils
						.getRequiredWebApplicationContext(requestAttributes.getRequest().getServletContext());

				tokenStore = (TokenStore) applicationContext.getBean("jwtTokenStore");
			}

			String authToken = authHeaderToken.substring("Bearer ".length());
			OAuth2AccessToken oauth2TokenDetails = tokenStore.readAccessToken(authToken);

			if (oauth2TokenDetails == null || oauth2TokenDetails.isExpired()) {

				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("User session does not exist..");

				return serviceStatus;
			} else {
				OAuth2Authentication oauth2Authentication = tokenStore.readAuthentication(oauth2TokenDetails);

				if (oauth2Authentication != null && oauth2Authentication.getUserAuthentication() != null
						&& oauth2Authentication.getUserAuthentication().getPrincipal() != null) {

					UserRepositoryUserDetails userDetails = null;
					if (oauth2Authentication.getUserAuthentication()
							.getPrincipal() instanceof UserRepositoryUserDetails) {
						userDetails = (UserRepositoryUserDetails) oauth2Authentication.getUserAuthentication()
								.getPrincipal();
					} else {
						String username = (String) oauth2Authentication.getUserAuthentication().getPrincipal();
						User user = userService.getUser(username);
						userDetails = new UserRepositoryUserDetails(user, user.getId());
					}
					Map<String, Object> result = new HashMap<>();

					String name = userDetails.getFirstName();
					result.put("userId", userDetails.getId());
					result.put("userType", userDetails.getUserRole().getUserType());
					if (userDetails.getMail() != null) {
						result.put("email", userDetails.getMail());
					} else {
						result.put("email", "");
					}
					result.put("mobile", userDetails.getMobile());
					
					if (userDetails.getLastName() != null && userDetails.getFirstName() != null) {
						name = userDetails.getFirstName() + " " + userDetails.getLastName();
					} else if (userDetails.getFirstName() != null) {
						name = userDetails.getFirstName();
					} else if (userDetails.getLastName() != null) {
						name = userDetails.getLastName();
					} else {
						name = "";
					}
					result.put("name", name);
					System.out.println("in suuccess");
					serviceStatus.setResult(result);
				}
			}
		} catch (Exception e) {
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception Occured");
		}
		return serviceStatus;
	}

	@RequestMapping(value = "/checkSessionOnPageRefresh", method = RequestMethod.POST, produces = {
			"application/json" }, consumes = { "application/json" })
	public ServiceStatus checkTokenOnPageRefresh(
			@RequestHeader(name = "Authorization", required = true) String authHeaderToken) {
		ServiceStatus serviceStatus = new ServiceStatus();
		serviceStatus.setStatus("success");
		serviceStatus.setMessage("Valid user session..");
		try{
		if (tokenStore == null) {
			ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
					.getRequestAttributes();
			WebApplicationContext applicationContext = WebApplicationContextUtils
					.getRequiredWebApplicationContext(requestAttributes.getRequest().getServletContext());

			tokenStore = (TokenStore) applicationContext.getBean("jwtTokenStore");
		}

		String authToken = authHeaderToken.substring("Bearer ".length());
		OAuth2AccessToken oauth2TokenDetails = tokenStore.readAccessToken(authToken);

		if (oauth2TokenDetails == null || oauth2TokenDetails.isExpired()) {

			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("User session does not exist..");

			return serviceStatus;
		} else {
			OAuth2Authentication oauth2Authentication = tokenStore.readAuthentication(oauth2TokenDetails);

			if (oauth2Authentication != null && oauth2Authentication.getUserAuthentication() != null
					&& oauth2Authentication.getUserAuthentication().getPrincipal() != null) {

				UserRepositoryUserDetails userDetails = null;
				if (oauth2Authentication.getUserAuthentication().getPrincipal() instanceof UserRepositoryUserDetails) {
					userDetails = (UserRepositoryUserDetails) oauth2Authentication.getUserAuthentication()
							.getPrincipal();
				} else {
					String username = (String) oauth2Authentication.getUserAuthentication().getPrincipal();
					User user = userService.getUser(username);
					userDetails = new UserRepositoryUserDetails(user, user.getId());
				}
				Map<String, Object> result = new HashMap<>();

				String name = userDetails.getFirstName();
				result.put("userId", userDetails.getId());
				result.put("userType", userDetails.getUserRole().getUserType());
				if (userDetails.getMail() != null) {
					result.put("email", userDetails.getMail());
				} else {
					result.put("email", "");
				}
				result.put("mobile", userDetails.getMobile());
				if (userDetails.getLastName() != null && userDetails.getFirstName() != null) {
					name = userDetails.getFirstName() + " " + userDetails.getLastName();
				} else if (userDetails.getFirstName() != null) {
					name = userDetails.getFirstName();
				} else if (userDetails.getLastName() != null) {
					name = userDetails.getLastName();
				} else {
					name = "";
				}
				result.put("name", name);
				serviceStatus.setResult(result);
			}
		}
		}catch(Exception e){
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

}
