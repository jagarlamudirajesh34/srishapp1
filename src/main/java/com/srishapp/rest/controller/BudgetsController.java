package com.srishapp.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.srishapp.modal.Budgets;
import com.srishapp.service.BudgetService;
import com.srishapp.util.ServiceStatus;

@RestController
@RequestMapping("/budgets")
public class BudgetsController {

	@Autowired
	BudgetService budgetService;

	// for getting all budgets details
	@RequestMapping(value = "/getAllActiveBudgets", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveBudgets() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Budgets> allBudgets = budgetService.getAllActiveBudgets();
			if (allBudgets != null && !allBudgets.isEmpty()) {
				serviceStatus.setMessage("Retrived all budgets records");
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allBudgets);
			} else {
				serviceStatus.setMessage("No Budget records found");
				serviceStatus.setStatus("failure");
			}
		} catch (Exception e) {
			serviceStatus.setMessage("execption occured");
			serviceStatus.setStatus("failure");
		}
		return serviceStatus;
	}

	// for getting total budget spent
	@RequestMapping(value = "/getTotalAmountSpent", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getTotalAmountSpent() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			Long total = budgetService.getTotalAmountSpent();
			serviceStatus.setMessage("retrived total amount spent");
			serviceStatus.setStatus("success");
			serviceStatus.setResult(total);
		} catch (Exception e) {
			serviceStatus.setMessage("execption occured");
			serviceStatus.setStatus("failure");
		}
		return serviceStatus;
	}
}
