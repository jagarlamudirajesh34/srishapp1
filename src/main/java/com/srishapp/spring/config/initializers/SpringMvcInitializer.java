package com.srishapp.spring.config.initializers;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.srishapp.security.OAuth2ServerConfiguration;
import com.srishapp.security.WebSecurityConfig;
import com.srishapp.spring.config.EmailConfig;


public class SpringMvcInitializer  extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {com.srishapp.spring.config.AppConfig.class, WebSecurityConfig.class, OAuth2ServerConfiguration.class, EmailConfig.class};
	}
	
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}
		
}