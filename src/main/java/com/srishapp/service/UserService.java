package com.srishapp.service;

import com.srishapp.modal.User;

public interface UserService {

	User getUser(String mobile);

}