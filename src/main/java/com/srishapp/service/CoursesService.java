package com.srishapp.service;

import java.util.List;

import com.srishapp.modal.Courses;

public interface CoursesService {

	List<Courses> getAllActiveCourses();

	List<Courses> getAllCourses();

	Courses getCourseById(int id);
}
