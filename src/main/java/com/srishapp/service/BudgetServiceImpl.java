package com.srishapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.srishapp.dao.BudgetsDao;
import com.srishapp.modal.Budgets;

@Service("budgetService")
@Transactional("transactionManager")
public class BudgetServiceImpl implements BudgetService {

	@Autowired
	BudgetsDao budgetsDao;

	@Override
	public List<Budgets> getAllActiveBudgets() {
		return budgetsDao.getAllActiveBudgets();
	}

	@Override
	public Long getTotalAmountSpent() {
		return budgetsDao.getTotalAmountSpent();
	}

}
