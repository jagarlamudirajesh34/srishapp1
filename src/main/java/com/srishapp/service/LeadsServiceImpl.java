package com.srishapp.service;

import java.util.Calendar;
import java.util.List;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CalcChainDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.srishapp.dao.LeadsDao;
import com.srishapp.modal.Enquiry;

@Service("leadsService")
@Transactional("transactionManager")
public class LeadsServiceImpl implements LeadsService {

	@Autowired
	LeadsDao leadsDao;

	@Override
	public List<Enquiry> getAllActiveLeads(String course) {
		Long courseId = (long) 1;
		if (course.equals("devops")) {
			courseId = (long) 1;
		} else if (course.equals("salesforce")) {
			courseId = (long) 2;
		} else if (course.equals("servicenow")) {
			courseId = (long) 3;
		} else if (course.equals("mule")) {
			courseId = (long) 4;
		}
		return leadsDao.getAllActiveLeads(courseId);
	}

	@Override
	public void saveLead(Enquiry enquiry) {
		enquiry.setCreatedDate(Calendar.getInstance().getTime());
		enquiry.setUpdatedDate(Calendar.getInstance().getTime());
		enquiry.setEnquiryBy(enquiry.getEnquiryBy().trim());
		enquiry.setName(enquiry.getName().trim());
		enquiry.setMobile(enquiry.getMobile().trim());
		enquiry.setWhatsapp(enquiry.getWhatsapp().trim());
		enquiry.setJoinedYn(false);
		leadsDao.saveOrUpdate(enquiry);
	}

}
