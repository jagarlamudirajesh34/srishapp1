package com.srishapp.service;

import java.util.List;

import com.srishapp.modal.Enquiry;

public interface LeadsService {
	List<Enquiry> getAllActiveLeads(String course);

	void saveLead(Enquiry enquiry);
}
