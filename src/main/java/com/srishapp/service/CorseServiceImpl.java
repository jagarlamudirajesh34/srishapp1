package com.srishapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.srishapp.dao.CoursesDao;
import com.srishapp.modal.Courses;

@Service("coursesService")
@Transactional("transactionManager")
public class CorseServiceImpl implements CoursesService {

	@Autowired
	CoursesDao coursesDao;

	@Override
	public List<Courses> getAllActiveCourses() {
		return coursesDao.getAllActiveCourses();
	}

	@Override
	public List<Courses> getAllCourses() {
		return coursesDao.getAllCourses();
	}

	@Override
	public Courses getCourseById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
