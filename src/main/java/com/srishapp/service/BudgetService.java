package com.srishapp.service;

import java.util.List;

import com.srishapp.modal.Budgets;

public interface BudgetService {

	List<Budgets> getAllActiveBudgets();

	Long getTotalAmountSpent();

}
