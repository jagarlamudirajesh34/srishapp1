package com.srishapp.modal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "enquiry")
public class Enquiry implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false, updatable = false)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "mobile", nullable = false)
	private String mobile;

	@Column(name = "mail", nullable = false)
	private String mail;

	@Column(name = "whatsapp", nullable = true)
	private String whatsapp;

	@Column(name = "user_comments", nullable = true)
	private String userComments;

	@Column(name = "staff_comments", nullable = true)
	private String staffComments;

	@ManyToOne
	@JoinColumn(name = "course_id")
	private Courses course;

	@Column(name = "enquiry_by", nullable = true)
	private String enquiryBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "deleted_yn")
	private Boolean deletedYn;
	
	@Column(name = "joined_yn")
	private Boolean joinedYn;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getWhatsapp() {
		return whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}

	public String getUserComments() {
		return userComments;
	}

	public void setUserComments(String userComments) {
		this.userComments = userComments;
	}

	public String getStaffComments() {
		return staffComments;
	}

	public void setStaffComments(String staffComments) {
		this.staffComments = staffComments;
	}

	public Courses getCourse() {
		return course;
	}

	public void setCourse(Courses course) {
		this.course = course;
	}

	public String getEnquiryBy() {
		return enquiryBy;
	}

	public void setEnquiryBy(String enquiryBy) {
		this.enquiryBy = enquiryBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getDeletedYn() {
		return deletedYn;
	}

	public void setDeletedYn(Boolean deletedYn) {
		this.deletedYn = deletedYn;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getJoinedYn() {
		return joinedYn;
	}

	public void setJoinedYn(Boolean joinedYn) {
		this.joinedYn = joinedYn;
	}

	@Override
	public String toString() {
		return "Enquiry [id=" + id + ", name=" + name + ", mobile=" + mobile + ", mail=" + mail + ", whatsapp="
				+ whatsapp + ", userComments=" + userComments + ", staffComments=" + staffComments + ", course="
				+ course + ", enquiryBy=" + enquiryBy + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate
				+ ", deletedYn=" + deletedYn + ", joinedYn=" + joinedYn + "]";
	}
}
