package com.srishapp.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.srishapp.modal.Enquiry;

@Repository("leadsDao")
public class LeadsDaoImpl extends AbstractDao implements LeadsDao {

	@Override
	public List<Enquiry> getAllActiveLeads(Long courseId) {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Enquiry.class);
		return (List<Enquiry>) criteria.add(Restrictions.eq("course.id", courseId)).list();
	}

}
