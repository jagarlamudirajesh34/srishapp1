package com.srishapp.dao;

import com.srishapp.modal.User;

public interface UserDao extends Dao {

	User getUser(String mobile);

}