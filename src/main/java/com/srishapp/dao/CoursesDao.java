package com.srishapp.dao;

import java.util.List;

import com.srishapp.modal.Courses;

public interface CoursesDao extends Dao {

	List<Courses> getAllActiveCourses();

	List<Courses> getAllCourses();

	Courses getCourseById(int id);

}
