package com.srishapp.dao;

import java.util.List;

import com.srishapp.modal.Enquiry;

public interface LeadsDao extends Dao {
	List<Enquiry> getAllActiveLeads(Long courseId);
}
