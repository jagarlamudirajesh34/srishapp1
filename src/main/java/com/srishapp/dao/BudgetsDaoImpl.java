package com.srishapp.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.srishapp.modal.Budgets;

@Repository("budgetsDao")
public class BudgetsDaoImpl extends AbstractDao implements BudgetsDao {

	@Override
	public List<Budgets> getAllActiveBudgets() {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Budgets.class);
		return (List<Budgets>) criteria.add(Restrictions.eq("deletedYn", false)).list();
	}

	@Override
	public Long getTotalAmountSpent() {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Budgets.class);
		criteria.setProjection(Projections.sum("amount"));
        return (Long) criteria.uniqueResult();
	}

}
