package com.srishapp.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.srishapp.modal.Courses;

@Repository("coursesDao")
public class CoursesDaoImpl extends AbstractDao implements CoursesDao {

	@Override
	public List<Courses> getAllActiveCourses() {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Courses.class);
		return (List<Courses>) criteria.add(Restrictions.eq("deletedYn", false)).list();
	}

	@Override
	public List<Courses> getAllCourses() {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Courses.class);
		return (List<Courses>) criteria.list();
	}

	@Override
	public Courses getCourseById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
