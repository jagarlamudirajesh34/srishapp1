package com.srishapp.dao;

import java.util.List;

import com.srishapp.modal.Budgets;

public interface BudgetsDao extends Dao {

	List<Budgets> getAllActiveBudgets();
	
	Long getTotalAmountSpent();

}
